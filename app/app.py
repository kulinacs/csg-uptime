import requests
import subprocess
from requests_file import FileAdapter
from flask import Flask, render_template, request

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html.j2')

@app.route('/isitup', methods=['GET'])
def isitup():
    get_url = request.args.get('url')
    content = None
    if get_url:
        s = requests.Session()
        s.mount('file://', FileAdapter())
        try:
            content = s.get(request.args.get('url')).text
        except:
            content = 'Error!'
    return render_template('isitup.html.j2', fill=content)

@app.route('/services', methods=['GET'])
def services():
    service = request.args.get('service')
    content = None
    if service:
        if ';' not in service:
            call = 'grep -m 1 ' + service + ' /etc/services'
            try:
                content = subprocess.check_output(call, shell=True).decode('ASCII')
            except:
                content = "Error!"
        else:
            content = "No service contains ; ..."
    return render_template('service.html.j2', fill=content)


if __name__ == '__main__':
    app.run()
