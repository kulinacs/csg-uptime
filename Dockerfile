FROM python:latest                             
MAINTAINER Nicklaus McClendon version: 0.1     

ADD app /app
WORKDIR /app

RUN pip3 install uwsgi
RUN pip3 install -r requirements.txt

EXPOSE 1337

CMD ["uwsgi", "--ini", "conf.ini"]
